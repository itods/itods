ItoDS
============
Here you can find the python scripts behind ItoDS. The scripts have been divided into  two files: `downloader.py`, which is used to obtain the data needed to train the model, and `model.py`, which is used for training the model and making predictions.

##### Needed libraries

Google APIs Client Library:
`pip install --upgrade google-api-python-client`

Additional libraries needed for authentication:
`pip install --upgrade google-auth google-auth-oauthlib google-auth-httplib2`

Other libraries needed:
`os`, `sys`, the Python science stack: `numpy`, `pandas`, `sklearn`, `matplotlib`

##### Other preparations

In order to be able to use `downloader.py`, you will need to acquire an OAuth 2.0 client ID and client secret from the Google Cloud Console at https://cloud.google.com/console and store the client secret file as `client_secret.json` in the same folder with the python scripts.

For more information about using OAuth2 to access the YouTube Data API, see: https://developers.google.com/youtube/v3/guides/authentication

Step by step instructions on activating YouTube Data API for use with Python can be found here: https://developers.google.com/youtube/v3/quickstart/python

##### Using downloader.py

Running `downloader.py` should result in the creation of folder `output` within the working directory and downloading a set of captions together with a `caption_data.json`-file containing the metadata. The queries used can be changed by editing the script. There are also a couple of example queries within the file. The default format for the captions will be `.srt`, but other formats supported by the API can be specified as well.

##### Using model.py

Running `model.py` builds a regression model on the data collected by `downloader.py`, outputs the features sorted by their weight (= effect on the target variable) in descending order. The output is saved in `output/feature_weights.csv`. The weights of the top and bottom 30 features are also visualized. As those features will include some irrelevant ones, the ignore_features list can be edited to filter them out from the visualization.

The variable to be predicted is by default likeRate, that is the ratio of likes and views. Other variables have not been as suitable for prediction in this data set.

The data is processed based on the following variables:
 - ngrams (tuple (int, int)). Include n-grams of these lengths, e.g., ngrams = (2,3) includes 2-grams and 3-grams.
 - tfidf (boolean). Whether the text data is tokenized with a CountVectorizer or a TfidfVectorizer. The latter seems to produce better results.
 - varthreshold (float). Features with variance (across videos) below this are discarded.

There is no guarantee that the model parameters (or the model itself) are optimal for some other data set. An optimization function will be added in a later version. Other regression models that may be better (or worse) are Lasso and ElasticNet, you may want to try those, too.

