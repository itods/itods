# -*- coding: utf-8 -*-
"""
"""

import matplotlib.pyplot as plt
import re
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import RidgeCV
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from dateutil.parser import parse
from sklearn.preprocessing import MaxAbsScaler
from sklearn.feature_selection import VarianceThreshold
import glob
import isodate
import scipy.sparse

# return captions in a list
def readCaptions():
    captionFiles = glob.glob("output/*srt")
    captionFiles = sorted(captionFiles, key=lambda x: int(re.sub('\D', '', x)))
    caption = [open(f, encoding='utf8').readlines() for f in captionFiles]
    captions = []
    for c in caption:
        s = ' '
        for line in c:
            if line[0].isalpha(): s += line.strip() + ' '
        captions.append(s)
    #caption = [c for c in x for x in caption if c[0].isalpha() ]
    #captions = [' '.join(open(f, encoding='utf8').readlines()) for f in captionFiles]
    
    #captions = [' '.join(open(f, encoding='utf8').readlines()[2::4]) for f in captionFiles]
    #captions = [c[:500] for c in captions]
    return captions
    

# parse duration from youtube data, return in seconds
def getDuration(x):
    return isodate.parse_duration(x).total_seconds()


# duration, like counts etc. from metadata
def readMetadata():
    df = pd.read_json(open('output/caption_data.json').read())    
    df['likeRate'] = 1.0*df['likeCount']/df['viewCount']    
    df['dislikeRate'] = 1.0*df['dislikeCount']/df['viewCount']    
    df['duration'] = [getDuration(x) for x in df.duration.values]    
    df['date'] = [parse(d) for d in df.publishedAt]
    df['commentCount'].fillna(0, inplace=True)
    df['commentRate'] = df['commentCount']/df['viewCount']
    df.set_index(np.array(df.index)+1, inplace=True)
    return df[['duration', 'date', 'likeCount', 'commentRate', 'dislikeRate', 'viewCount', 'likeRate', 'CH_subscriberCount']]


def speechRate(captions, metadata):
    sr = []
    for i in range(len(captions)):
        words = len(captions[i].split(' '))
        sr.append(words/metadata.iloc[i].duration)
    return sr


# add feature = column to X matrix
def addFeature(X, new_feature):
    new_feature = scipy.sparse.csr_matrix(new_feature.reshape(new_feature.shape[0], 1))
    return scipy.sparse.csr_matrix(scipy.sparse.hstack([X, new_feature]))


def getData(captions, metadata, varthreshold, ngrams, y_feature='likeRate'):
    # remove ngrams and tokenize
    if tfidf: 
        vect = TfidfVectorizer(stop_words='english', ngram_range=ngrams).fit(captions)
    else: 
        vect = CountVectorizer(stop_words='english', ngram_range=ngrams).fit(captions)
    X = vect.transform(captions)
    print("Features initially:", X.shape[1])
    
    # we want to get rid of features with the lowest variance,
    # they are likely to be irrelevant
    X = MaxAbsScaler().fit_transform(X)
    vt = VarianceThreshold(varthreshold).fit(X)
    mask = vt.get_support() # boolean array of the original ngrams we chose
    X = vt.transform(X)
    print("Features remaining:", X.shape[1])
    
    y = metadata[y_feature]
    
    return X,y, mask


# TBD: find the optimal parameter values (tfidf, ngrams, varthreshold)
def optimize():
    raise NotImplementedError("Not implemented yet.")


def visualize_weights(feature_weights, n_features=30):
    names = np.array(feature_weights.index)
    weights = feature_weights.ravel()
    pos = weights[:n_features]
    neg = weights[-n_features:]
    pos_neg = np.hstack([pos, neg])
    names = np.hstack([names[:n_features], names[-n_features:]])
    
    plt.figure(figsize=(15, 5))
    colors = ['r' if w < 0 else 'b' for w in pos_neg]
    plt.bar(np.arange(2*n_features), pos_neg, color=colors)
    
    plt.subplots_adjust(bottom=0.4)
    plt.xticks(np.arange(1, 1 + 2 * n_features),
               names, rotation=60,
               ha="right")
    plt.ylabel("weight")
  

if __name__ == "__main__":
    captions = readCaptions()
    metadata = readMetadata()

    opt = False
    # tried and true parameter values 
    tfidf = True
    varthreshold = 0.007
    ngrams = (2,4)

    print("tfidf:", tfidf)
    print("ngrams:", ngrams)
    print("variance threshold:", varthreshold)

    # split the data and fit the model
    X, y, selected_features = getData(captions, metadata, varthreshold, ngrams)
    
    X_train, X_test, y_train, y_test = train_test_split(X, y)

    # ridge regression with cross validation
    ridge = RidgeCV().fit(X_train, y_train)
    print('Test score:', ridge.score(X_test, y_test))


    # sort the ngrams according to their coefficient
    if tfidf: 
        features = TfidfVectorizer(stop_words='english', ngram_range=ngrams).fit(captions).get_feature_names()
    else:
        features = CountVectorizer(stop_words='english', ngram_range=ngrams).fit(captions).get_feature_names()

    feature_subset = np.array(features)[selected_features]
    feature_weights = pd.Series(ridge.coef_, index=feature_subset).sort_values(ascending=False)

    # remove the following entries
    ignore_features = ['liked', 'probably don', 've', '20', 'let run',
                       'gonna run', 'okay let', 'let', 'going run',
                       'gonna use', 'going install', 've got']
    for f in ignore_features:
        try: del feature_weights[f]
        except KeyError: pass

    feature_weights.to_csv('output/feature_weights.csv')

    visualize_weights(feature_weights)
    
    
    



