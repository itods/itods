# -*- coding: utf-8 -*-
#!/usr/bin/python

import httplib2
import os
import sys
import pandas as pd
import numpy as np

from apiclient.discovery import build
from apiclient.errors import HttpError
from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
from oauth2client.tools import argparser, run_flow


# The CLIENT_SECRETS_FILE variable specifies the name of a file that contains
# the OAuth 2.0 information for this application, including its client_id and
# client_secret. You can acquire an OAuth 2.0 client ID and client secret from
# the {{ Google Cloud Console }} at
# {{ https://cloud.google.com/console }}.
# Please ensure that you have enabled the YouTube Data API for your project.
# For more information about using OAuth2 to access the YouTube Data API, see:
#   https://developers.google.com/youtube/v3/guides/authentication
# For more information about the client_secrets.json file format, see:
#   https://developers.google.com/api-client-library/python/guide/aaa_client_secrets
CLIENT_SECRETS_FILE = "client_secret.json"

# This OAuth 2.0 access scope allows for full read/write access to the
# authenticated user's account and requires requests to use an SSL connection.
YOUTUBE_READ_WRITE_SSL_SCOPE = "https://www.googleapis.com/auth/youtube.force-ssl"
API_SERVICE_NAME = "youtube"
API_VERSION = "v3"

# This variable defines a message to display if the CLIENT_SECRETS_FILE is
# missing.
MISSING_CLIENT_SECRETS_MESSAGE = """
WARNING: Please configure OAuth 2.0

To make this sample run you will need to populate the client_secrets.json file
found at:
   %s
with information from the APIs Console
https://console.developers.google.com

For more information about the client_secrets.json file format, please visit:
https://developers.google.com/api-client-library/python/guide/aaa_client_secrets
""" % os.path.abspath(os.path.join(os.path.dirname(__file__),
                                   CLIENT_SECRETS_FILE))

# Authorize the request and store authorization credentials.
def get_authenticated_service(args):
    flow = flow_from_clientsecrets(CLIENT_SECRETS_FILE, scope=YOUTUBE_READ_WRITE_SSL_SCOPE,
                                   message=MISSING_CLIENT_SECRETS_MESSAGE)

    storage = Storage("%s-oauth2.json" % sys.argv[0])
    credentials = storage.get()

    if credentials is None or credentials.invalid:
        credentials = run_flow(flow, storage, args)

    # Trusted testers can download this discovery document from the developers page
    # and it should be in the same directory with the code.
    return build(API_SERVICE_NAME, API_VERSION,
                 http=credentials.authorize(httplib2.Http()))

args = argparser.parse_args()
service = get_authenticated_service(args)

# Call the API's videos.list method to recover information on a video.
def get_video_data(service, video_id):
    results = service.videos().list(
            part = 'snippet,contentDetails,statistics',
            id = video_id
            ).execute()
    items = results['items'][0]
    data = {}
    data.update(items['snippet'])
    data.update(items['contentDetails'])
    data.update(items['statistics'])
    return data

# Call the API's captions.list method to obtain a list of existing caption tracks for
# the video given as a parameter.
def list_captions(service, video_id):
    results = service.captions().list(
            part = 'snippet',
            videoId = video_id
            ).execute()
    return results['items']

# Call the API's captions.download method to download an existing caption track.
def download_caption(service, caption_id, tfmt):
    subtitle = service.captions().download(
            id = caption_id,
            tfmt = tfmt
            ).execute()
    return subtitle

# Get metadata for the channel given as a parameter.
def get_channel_data(service, channel_id):
    channel_data = service.channels().list(
            part = 'statistics',
            id = channel_id
            ).execute()
    return channel_data['items'][0]

''' Returns a list of video_ids that match the query given as a parameter.
    maxResults sets the number of results returned, with a maximum of 50.
    If more results are needed, the nPages parameter can be used to fetch more
    pages of results. In this case, the total number of results obtained is
    nPages * maxResults
'''
    
def search_by_query(service, query, maxResults = 50, nPages = 1):
    video_ids = []
    result = service.search().list(
            part = 'snippet',
            maxResults = maxResults,
            q = query,
            type = 'video'
            ).execute()
    video_ids = video_ids + [item['id']['videoId'] for item in result['items']]
    for i in range(nPages - 1):
        pageToken = result['nextPageToken']
        result = service.search().list(
                part = 'snippet',
                maxResults = maxResults,
                q = query,
                pageToken = pageToken,
                type = 'video'
                ).execute()
        video_ids = video_ids + [item['id']['videoId'] for item in result['items']]
    return video_ids

# A function to print a progress bar in the console.
# Obtained from https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total: 
        print()

''' Downloads captions for videos matching given queries or specified in
    video_ids and stores them into files. Also creates a json file containing
    metadata associated with the videos.
    
    Parameter queries can be either a list of strings to be used as queries or
    a list of (string, int) tuples, where the int specifies the number of pages
    of results to be fetched for that specific query.
    
    video_ids can be used to specify single videos. Both queries and video_ids
    can be used in the same call.
    
    maxResults sets the number of results fetched per page. The API limits
    the maximum to 50.
    
    nPages specifies how many pages of results should be fetched per query.
    If the (string, int) format for the queries is used, this parameter is
    overridden.
    
    Resulting captions are stored in separate files under directory output,
    which will be created if it doesn't exist already. Caption metadata will be
    stored in the same directory in json format as caption_data.json
    
    The function also returns the metadata dataFrame when it is finished for
    immediate processing. If you forget to assign the returned value to a
    variable it is recommended to read the data from the created file instead
    of repeating the download.
'''
def download_data(service, queries = [], video_ids = [], maxResults = 50, pages = 1):

    video_fields = ['title', 'description', 'duration', 'definition', 'publishedAt', 'likeCount', 'viewCount', 'dislikeCount', 'favoriteCount', 'commentCount', 'channelId', 'channelTitle']
    caption_fields = ['trackKind']
    channel_fields = ['subscriberCount', 'viewCount']
    cap_format = 'srt'
    numeric_fields = ['likeCount', 'viewCount', 'CH_subscriberCount', 'CH_viewCount', 'dislikeCount', 'favoriteCount', 'commentCount']
    path = 'output/'

    videos = dict()

    # Build a dictionary out of the search results using different queries
    if len(queries) > 0:
        if type(queries[0]) == str:
            for query in queries:
                for id in search_by_query(service, query, maxResults, pages):
                    if id not in videos.keys():
                        videos[id] = set([query])
                    else:
                        videos[id].add(query)
                        
        elif type(queries[0]) == tuple:
            for query, npages in queries:
                for id in search_by_query(service, query, maxResults, npages):
                    if id not in videos.keys():
                        videos[id] = set([query])
                    else:
                        videos[id].add(query)
            
        else:
            print('Error: Incorrect query format')
            return

    # Add hand-picked videos to the dictionary
    for id in video_ids:
        if id not in videos.keys():
            videos[id] = set(['hand-picked'])
        else:
            videos[id].add('hand-picked')

    # Create output directory
    if not os.path.exists(path):
        os.makedirs(path)

    # Reserve memory for the dataframe
    data = pd.DataFrame(index = np.arange(1, len(videos.keys()) + 1), columns = ['query', 'video_id', 'caption'] + video_fields + caption_fields + ['CH_' + i for i in channel_fields])

    i = 1
    printProgressBar (0, len(videos.keys()), prefix = 'Progress:', suffix = 'Complete', length = 50)
    for n, video_id in enumerate(videos.keys()):
        printProgressBar (n + 1, len(videos.keys()), prefix = 'Progress:', suffix = 'Complete, {} captions obtained'.format(i - 1), length = 50)
        # Try to fetch caption metadata and the caption file. If either fails, continue to the next video
        try:
            caption_data = list_captions(service, video_id)[0]
            # If caption is not in English, skip to the next file
            if (caption_data['snippet']['language'] != 'en') or (caption_data['snippet']['status'] == 'failed'):
                continue
            caption = download_caption(service, caption_id = caption_data['id'], tfmt = cap_format)
            
        except:
            continue

        filename = 'caption' + str(i) + '.' + cap_format
        # Store the caption into a file
        f = open(path + filename, 'wb')
        f.write(caption)
        f.close()

        # Add the query info to the dataframe
        data.loc[i, 'query'] = videos[video_id]
        
        # Add the video_id to the dataframe
        data.loc[i, 'video_id'] = video_id
        
        # Add the caption filename to the dataframe
        data.loc[i, 'caption'] = filename

        # Add the caption metadata to the dataFrame
        for field in caption_fields:
            try:
                data.loc[i, field] = caption_data['snippet'][field]
            except KeyError:
                continue

        # Fetch the video metadata
        video_data = get_video_data(service, video_id)

        # Store the video metadata into the dataframe
        for field in video_fields:
            try:
                data.loc[i, field] = video_data[field]
            except KeyError:
                continue

        # Fetch the channel metadata for the video
        channel_data = get_channel_data(service, video_data['channelId'])

        # Add the channel metadata to the dataFrame
        for field in channel_fields:
            try:
                data.loc[i, 'CH_' + field] = channel_data['statistics'][field]
            except KeyError:
                continue
        i += 1

    # Remove unused rows from the end
    data = data.loc[: i - 1]

    # Change count columns from strings into numeric
    data[numeric_fields] = data[numeric_fields].apply(pd.to_numeric)

    # Write the data to a json-file
    data.to_json(path + 'caption_data.json', orient='records')
    return data



''' Used only for adding new data fields to data fetched earlier
    This function is used to add new data fields to already existing data.
    This is not needed in normal use.
'''
def add_new_fields(service, data, field_names, numeric = False):
    missing_fields = set()
    for field in field_names:
        if field not in data.keys():
            missing_fields.add(field)
    missing_fields = list(missing_fields)
    for field in missing_fields:
        data[field] = np.NaN
    printProgressBar (0, len(data), prefix = 'Progress:', suffix = 'Complete', length = 50)
    for i, video_id in enumerate(data['video_id']):
        printProgressBar (i + 1, len(data), prefix = 'Progress:', suffix = 'Complete', length = 50)
        video_data = get_video_data(service, video_id)
        for field in missing_fields:
            try:
                data.loc[i, field] = video_data[field]
            except KeyError:
                continue
    if numeric:
        data[missing_fields] = data[missing_fields].apply(pd.to_numeric)

# This is a simple demonstration query
queries = ['NLTK tutorial']

# This is the query used for obtaining the data for the pitch. Using this query
# will take a few minutes and use up a large portion of the default quota for
# using the API.

large_queries = [('NLTK tutorial', 10), ('python machine learning tutorial', 10), ('NLTK', 8), ('NLP tutorial -"neuro linguistic programming"', 6), ('python deep learning tutorial', 8), ('python machine learning tutorial -"sentdex" -"siraj raval"', 8)]

# This will download the captions and the metadata and store everything to files
download_data(service, queries)
